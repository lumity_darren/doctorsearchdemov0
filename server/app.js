const express = require('express')
const app = express()
const port = 5000
const axios = require('axios')
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.get('/', (req, res) => res.send('Hello World!'))

app.get('/api/sandbox',(req,res)=>{
  const headers= { 
		"x-api-key": "kG5t0C4Fl0P05L1fw644ZGLn16q64D2GH1ZG6LvZWzg",
		// "Accept-Encoding": 'gzip, deflate',
		// "Accept": '*/*' 
  }
  const query = req.query;
	axios.get('https://sandbox.providernexus.com/api/v6/projects/20171359/specialtyGroups', {headers:headers, params: query})
		.then(function (response) {
			// handle success
      console.log(response);
      res.send(response.data);
		})
		.catch(function (error) {
			// handle error
      console.log(error);
      res.send(error);
		});

});
app.listen(port, () => console.log(`Example app listening on port ${port}!`))